# Projet de structuration d'une association
## Objectif
Recycler du matériel informatique jugé obsolète par leur propriétaire
avec des système d'exploitation et logiciels Libres.

Le matériel ainsi réhabilité sera donné à des associations à but non lucratif, 
à des institutions ou des particuliers.

## Structuration
La première réflexion active de structuration a été menée le vendredi 30 septembre
2016 de 14:30 à 16:00 sur le cours de Si1 avec Grégory DAVID.

![Carte mentale de structuration](Association_Reconditionnement.png)